import sys
import math


def read_file(file_name):
    f = open(file_name, "rb")
    header1 = f.read(14)
    bf_off_bits_bytes = header1[10:]
    bf_off_bits: int = int.from_bytes(bf_off_bits_bytes, byteorder='little', signed=False)
    bi_size_bytes = f.read(4)
    bi_size: int = int.from_bytes(bi_size_bytes, byteorder='little', signed=False)
    header2 = f.read(bi_size - 4)
    bi_bit_count_bytes = header2[10:12]
    bi_bit_count = int.from_bytes(bi_bit_count_bytes, byteorder='little', signed=True)
    header3 = f.read(bf_off_bits - bi_size - 14)
    data = f.read()
    f.close()
    f = open(file_name, "rb")
    head = f.read(bf_off_bits)
    f.close()
    return data, head


def write_file(file_name, new_data, head):
    f = open(file_name, "wb")
    f.write(head)
    f.write(new_data)
    f.close()
    return


def change_to_yiq(file_name):
    data, head = read_file(file_name)
    new_file_name = file_name[0:-4] + "-1160300528-YIQ.bmp"
    data_array = bytearray(data)
    i = 0
    while i < data_array.__len__():
        b = data_array[i]
        g = data_array[i + 1]
        r = data_array[i + 2]
        t = int(0.299 * r + 0.587 * g + 0.114 * b)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i] = t
        t = int(0.596 * r - 0.274 * g - 0.322 * b)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i + 1] = t
        t = int(0.211 * r - 0.523 * g + 0.312 * b)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i + 2] = t
        i = i + 3
    write_file(new_file_name, data_array, head)
    return


def change_to_ycbcr(file_name):
    data, head = read_file(file_name)
    new_file_name = file_name[0:-4] + "-1160300528-YCbCr.bmp"
    data_array = bytearray(data)
    i = 0
    while i < data_array.__len__():
        b = data_array[i]
        g = data_array[i + 1]
        r = data_array[i + 2]
        t = int(0.299 * r + 0.587 * g + 0.114 * b)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i] = t
        t = 128 + int(-0.169 * r - 0.331 * g - 0.500* b)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i + 1] = t
        t = 128 + int(0.500 * r - 0.419 * g + 0.081 * b)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i + 2] = t
        i = i + 3
    write_file(new_file_name, data_array, head)
    return


def change_to_hsi(file_name):
    data, head = read_file(file_name)
    new_file_name = file_name[0:-4] + "-1160300528-HSI.bmp"
    data_array = bytearray(data)
    i = 0
    while i < data_array.__len__():
        b = data_array[i]
        g = data_array[i + 1]
        r = data_array[i + 2]
        if float(r + g + b) == 0:
            data_array[i] = 0
            data_array[i+1] = 0
            data_array[i+2] = 0
            i = i + 3
            continue
        else:
            r0 = r / float(r + g + b)
            b0 = b / float(r + g + b)
            g0 = g / float(r + g + b)

        m0 = 0.5 * ((r0 - b0) + (r0 - g0))
        m1 = (r0 - g0) * (r0 - g0) + (r0 - b0) * (g0 - b0)
        m2 = math.sqrt(m1)
        if m2 == 0:
            if m0 > 0:
                m4 = 0
            else:
                m4 = math.pi
        else:
            m3 = m0 / m2
            m4 = math.acos(m3)
            if m4 > math.pi:
                m4 = math.pi
            elif m4 < 0:
                m4 = 0
        h: float
        if b <= g:
            h = m4
        else:
            h = 2 * math.pi - m4
        s = 1 - 3 * min(r0, g0, b0)
        t = int(h * 180 / math.pi)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i] = t
        t = int(s * 100)
        if t > 255:
            t = 255
        elif t < 0:
            t = 0
        data_array[i + 1] = t
        t = int((r + b + g) / 3)
        data_array[i + 2] = t
        i = i + 3
    write_file(new_file_name, data_array, head)
    return


def change_to_xyz(file_name):
    data, head = read_file(file_name)
    new_file_name = file_name[0:-4] + "-1160300528-XYZ.bmp"
    data_array = bytearray(data)
    i = 0
    while i < data_array.__len__():
        b = data_array[i]
        g = data_array[i + 1]
        r = data_array[i + 2]
        t = int(0.412453 * r + 0.357580 * g + 0.180423 * b)
        if t > 255:
            t = 255
        data_array[i] = t
        t = int(0.212671 * r + 0.715160 * g + 0.1072169 * b)
        if t > 255:
            t = 255
        data_array[i + 1] = t
        t = int(0.019334 * r + 0.119193 * g + 0.950227 * b)
        if t > 255:
            t = 255
        data_array[i+2] = t
        i = i + 3
    write_file(new_file_name, data_array, head)
    return



def main(file_name):
    change_to_xyz(file_name)
    change_to_yiq(file_name)
    change_to_ycbcr(file_name)
    change_to_hsi(file_name)
    return



if __name__ == '__main__':
    path = sys.argv[1]
    main(path)
    