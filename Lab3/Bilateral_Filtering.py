from PIL import Image
import numpy as np
import math


win_m: int = 15
win_n: int = 15
half_m: int = int((win_m - 1) / 2)
half_n: int = int((win_n - 1) / 2)
sigma_d = 20
sigma_r = 35


def cut_t(t, top, bot):
    if t > top:
        t = top
    elif t < bot:
        t = bot
    return t


def cal_w(image, i, j, k, l):
    pix_1 = image.getpixel((i, j))
    pix_2 = image.getpixel((k, l))
    m1 = 0
    for counter in range(3):
        m1 = m1 + (pix_1[counter] - pix_2[counter]) ** 2
    m2 = m1
    m3 = m2 / (2 * (sigma_r ** 2))
    m4 = ((i - k) ** 2) + ((j - l) ** 2)
    m5 = m4 / (2 * (sigma_d ** 2))
    m6 = - m5 - m3
    m7 = math.exp(m6)
    return m7


def bilateral_filtering(filename):
    image = Image.open(filename)
    image.convert("RGB")
    image_2 = image.copy()
    x = image.size[0]
    y = image.size[1]

    for i in range(half_m, x - half_m):
        for j in range(half_n, y - half_n):
            pix_sum = np.zeros(3, dtype=float)
            pix_new = np.zeros(3, dtype=int)
            w_sum = 0
            flag = False
            for k in range(i - half_m, i + half_m + 1):
                for l in range(j - half_n, j + half_n + 1):
                    if k == i and l == j:
                        flag = True
                        continue
                    w = cal_w(image, i, j, k, l)
                    pix_kl = image.getpixel((k, l))
                    for counter in range(3):
                        pix_sum[counter] = pix_sum[counter] + pix_kl[counter] * w
                    w_sum = w_sum + w
            for counter in range(3):
                t = int(pix_sum[counter] / w_sum)
                t = cut_t(t, 255, 0)
                pix_new[counter] = t
            image.putpixel((i, j), (pix_new[0], pix_new[1], pix_new[2]))

    image.save(filename[0:-4] + "_bilateral_filtering" + filename[-4:])
    return


bilateral_filtering("13.png")
