from PIL import Image
import numpy as np


def robert_counter(filename, th):
    image = Image.open(filename)
    image_l = image.convert("L")
    x = image.size[0]
    y = image.size[1]
    s = np.zeros([x, y])
    for i in range(0, x - 1):
        for j in range(0, y - 1):
            a = []
            k_list = np.zeros(4)
            k = 0
            for m in range(i, i + 2):
                for n in range(j, j + 2):
                    k_list[k] = image_l.getpixel((m, n))
                    k = k + 1
            m1 = k_list[0] - k_list[3]
            m2 = k_list[1] - k_list[2]
            m3 = m1**2 + m2 ** 2
            m4 = m3 ** 0.5
            s[i, j] = m4
    th1 = th * np.max(s)
    for i in range(1, x - 1):
        for j in range(1, y - 1):
            if s[i, j] >= th1:
                image_l.putpixel((i, j), 255)
            else:
                image_l.putpixel((i, j), 0)
    image_l.save(filename[0:-4] + "_robert" + filename[-4:])
    return


def sobel_counter(filename, th):
    image = Image.open(filename)
    image_l = image.convert("L")
    x = image.size[0]
    y = image.size[1]
    s = np.zeros([x, y])
    for i in range(1, x - 1):
        for j in range(1, y - 1):
            a = []
            k_list = np.zeros(9)
            k = 0
            for m in range(i - 1, i + 2):
                for n in range(j - 1, j + 2):
                    k_list[k] = image_l.getpixel((m, n))
                    k = k + 1
            m1 = k_list[0] + 2 * k_list[1] + k_list[2]
            m2 = k_list[6] + 2 * k_list[5] + k_list[4]
            m3 = k_list[0] + 2 * k_list[7] + k_list[6]
            m4 = k_list[2] + 2 * k_list[3] + k_list[4]
            m5 = (m1 - m2) ** 2
            m6 = (m3 - m4) ** 2
            m7 = (m5 + m6) ** 0.5
            s[i, j] = m7
    th1 = th * np.max(s)
    for i in range(1, x - 1):
        for j in range(1, y - 1):
            if s[i, j] >= th1:
                image_l.putpixel((i, j), 255)
            else:
                image_l.putpixel((i, j), 0)
    image_l.save(filename[0:-4] + "_sobel" + filename[-4:])
    return


sobel_counter("1.jpg", 0.1)
