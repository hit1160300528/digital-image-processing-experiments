from PIL import Image
import numpy as np
from matplotlib import pyplot as plt


def ave(filename):
    image = Image.open(filename)
    image2 = image.copy()
    x = image.size[0]
    y = image.size[1]
    for i in range(1, x - 1):
        for j in range(1, y - 1):
            a = []
            for m in range(i - 1, i + 2):
                for n in range(j - 1, j + 2):
                    a.append(image.getpixel((m, n)))
            p = [0, 0, 0]
            for k in range(9):
                for rgb_k in range(3):
                    p[rgb_k] = p[rgb_k] + a[k][rgb_k]
            for k in range(3):
                p[k] = int(p[k] / 9)
            image2.putpixel((i, j), (p[0], p[1], p[2]))
    image2.save(filename[0:-4] + "_ave" + filename[-4:])


def med(filename):
    image = Image.open(filename)
    image2 = image.copy()
    image_l = image.convert("L")
    x = image.size[0]
    y = image.size[1]
    for i in range(1, x - 1):
        for j in range(1, y - 1):
            a = []
            sort_list = np.zeros(9)
            k = 0
            for m in range(i - 1, i + 2):
                for n in range(j - 1, j + 2):
                    a.append(image.getpixel((m, n)))
                    sort_list[k] = image_l.getpixel((m, n))
                    k = k + 1
            l = np.argwhere(sort_list == np.median(sort_list))[0][0]
            image2.putpixel((i, j), (a[l][0], a[l][1], a[l][2]))
    image2.save(filename[0:-4] + "_med" + filename[-4:])


def histogram(filename, channels):
    l = 256 / channels
    image = Image.open(filename)
    image_l = image.convert("L")
    x = image.size[0]
    y = image.size[1]
    his = np.zeros(channels)
    for i in range(1, x - 1):
        for j in range(1, y - 1):
            k = image_l.getpixel((i, j))
            index_channels = int(k / l)
            his[index_channels] = his[index_channels] + 1
    plt.bar(np.arange(channels), his)
    plt.show()
    return his


histogram("1.jpg", 32)

