from PIL import Image
import numpy as np


win_m: int = 3
win_n: int = 3
half_m: int = 1
half_n: int = 1
th = int((win_m * win_n + 1) / 2)
cha_len = 1


def pis(x, y):
    x0 = x - half_m
    x1 = x + half_m
    y0 = y - half_n
    y1 = y + half_n
    return x0, x1, y0, y1


def histogram(x, y, image):
    x0, x1, y0, y1 = pis(x, y)
    image_l = image.convert("L")
    his = np.zeros(int(256/cha_len))
    for i in range(x0, x1 + 1):
        for j in range(y0, y1 + 1):
            k = image_l.getpixel((i, j))
            index_channels = int(k/cha_len)
            his[index_channels] = his[index_channels] + 1
    return his


def change_his(x, y, image_l, his, m_num, med):
    x0, x1, y0, y1 = pis(x, y)
    for j in range(y0, y1 + 1):
        k0 = image_l.getpixel((x0 - 1, j))
        k1 = image_l.getpixel((x1, j))
        index_channels = int(k0 / cha_len)
        his[index_channels] = his[index_channels] - 1
        if k0 < med:
            m_num = m_num - 1
        index_channels = int(k1 / cha_len)
        his[index_channels] = his[index_channels] + 1
        if k1 < med:
            m_num = m_num - 1
    return his, m_num


def change_pix(image_l, image, x, y, med):
    x0, x1, y0, y1 = pis(x, y)
    for i in range(x0, x1 + 1):
        flag = False
        for j in range(y0, y1 + 1):
            k = image_l.getpixel((i, j))
            if int(k/cha_len) == med:
                flag = True
                break
        if flag:
            break
    a = image.getpixel((i, j))
    image.putpixel((x, y), (a[0], a[1], a[2]))

    return


def change_med(his, m_num, med):
    if m_num == th:
        return med, m_num
    if m_num > th:
        while m_num > th:
            m_num = m_num - his[med]
            med = med - 1
        if m_num < th:
            med = med + 1
            m_num = m_num + his[med]
    else:
        while m_num < th:
            med = med + 1
            m_num = m_num + his[med]
    return med, m_num


def init_med(his, m_num, med):
    counter = 0
    for i in range(len(his)):
        counter = counter + his[i]
        if counter >= th:
            med = i
            break

    return med, counter


def quick_med(filename):
    image = Image.open(filename)
    image_l = image.convert("L")
    x = image.size[0]
    y = image.size[1]
    for j in range(half_n, y - half_n):
        his = histogram(half_m, j, image_l)
        med = 0
        m_num = 0
        med, m_num = init_med(his, m_num, med)
        change_pix(image_l, image, half_m, j, med)
        for i in range(half_m + 1, x - half_m):
            his, m_num = change_his(i, j, image_l, his, med, m_num)
            med, m_num = init_med(his, m_num, med)
            change_pix(image_l, image, i, j, med)
    image.save(filename[0:-4] + "_quimid" + filename[-4:])
    return


quick_med("2.png")
