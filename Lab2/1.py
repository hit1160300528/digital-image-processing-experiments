from PIL import Image
import numpy as np


def cut_t(t, top, bot):
    if t > top:
        t = top
    elif t < bot:
        t = bot
    return t


def brit(filename, a):
    image = Image.open(filename)
    x = image.size[0]
    y = image.size[1]
    p = np.zeros(3, dtype=int)
    for i in range(0, x):
        for j in range(0, y):
            p[0], p[1], p[2] = image.getpixel((i, j))
            for k in range(3):
                t = int(p[k] * a)
                t = cut_t(t, 255, 0)
                p[k] = int(t)
            image.putpixel((i, j), (p[0], p[1], p[2]))
    image.save(filename[0:-4] + "_bri" + filename[-4:])
    return


def change_contrast(filename, a):
    image = Image.open(filename)
    image.convert("RGB")
    image_l = image.convert("L")
    x = image.size[0]
    y = image.size[1]
    p = np.zeros(3, dtype=int)
    p_change = np.zeros(3, dtype=int)
    med = np.zeros(3, dtype=int)
    for i in range(0, x):
        for j in range(0, y):
            p_change[0], p_change[1], p_change[2] = image.getpixel((i, j))
            for k in range(3):
                med[k] = med[k] + p_change[k]
    for k in range(3):
        med[k] = med[k] / (x * y)
    for i in range(0, x):
        for j in range(0, y):
            p[0], p[1], p[2] = image.getpixel((i, j))
            for k in range(3):
                t = int((p[k] - med[k]) * a + med[k])
                t = cut_t(t, 255, 0)
                p[k] = int(t)
            image.putpixel((i, j), (p[0], p[1], p[2]))
    image.save(filename[0:-4] + "_contrast" + filename[-4:])
    return


def change_saturation(filename, a):
    image = Image.open(filename)
    image = image.convert("RGB")
    x = image.size[0]
    y = image.size[1]
    p = np.zeros(3, dtype=int)
    for i in range(0, x):
        for j in range(0, y):
            p[0], p[1], p[2] = image.getpixel((i, j))
            flag_max = 0
            for k in range(1, 3):
                if p[k] < p[flag_max]:
                    flag_max = k
            flag_min = 0
            for k in range(1, 3):
                if p[k] > p[flag_min]:
                    flag_min = k
            flag_med = 0
            for k in range(1, 3):
                if k != flag_max and k != flag_min:
                    flag_med = k
            t = int((p[flag_max] - p[flag_med]) * a + p[flag_med])
            t = cut_t(t, 255, 0)
            p[flag_max] = int(t)
            t = int(p[flag_med] - (p[flag_med] - p[flag_min]) * a)
            t = cut_t(t, 255, 0)
            p[flag_min] = int(t)
            image.putpixel((i, j), (p[0], p[1], p[2]))
    image.save(filename[0:-4] + "_saturation" + filename[-4:])
    return


change_contrast("1.jpg", 1.25)
